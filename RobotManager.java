package youngsters2;

public interface RobotManager {

	RobotCommand getNextCommand();
	
	boolean sendCommand(RobotCommand command);
	
	boolean isComplete();

}

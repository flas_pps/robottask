package youngsters2;

public class Main {

	public static void main()
	{
		Robot robot = new Robot(config, position, direction);
		RobotManager robotManager = new RobotManager(robot);
		
		while (true)
		{
			if (robotManager.isComplete())
				break;
			
			RobotCommand command = robotManager.getNextCommand();
			robotManager.sendCommand(command);
		}
	}
	
}

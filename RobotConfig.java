package youngsters2;

public class RobotConfig {

	public int m_maxSpeed, m_maxRotation;
	
	public RobotConfig(int maxSpeed, int maxRotation)
	{
		m_maxSpeed = maxSpeed;
		m_maxRotation = maxRotation;
	}
}

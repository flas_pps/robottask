package youngsters2;

public class RobotCommand {

	public Vec2 dt_rotation;
	public Vec2 dt_speed;
	
	public RobotCommand(Vec2 _dt_rotation, Vec2 _dt_speed)
	{
		dt_rotation = _dt_rotation;
		dt_speed = _dt_speed;
	}
	
}

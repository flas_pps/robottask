package youngsters2;

public class Robot {
	
	private RobotConfig m_config;
	private Vec2 m_position, m_direction;
	
	Robot(RobotConfig config, Vec2 position, Vec2 direction)
	{
		m_config = config;
		m_position = position;
		m_direction = direction;
	}
	
	public RobotConfig getConfig() { return m_config; }
	public Vec2	getPosition() { return m_position; }
	public Vec2	getDirection() { return m_direction; }
	
	private void setPosition(Vec2 position) { m_position = position; }
	private void setDirection(Vec2 direction) { m_direction = direction; }
	
	public void execute(RobotCommand command)
	{
		// command handling
	}
	
}

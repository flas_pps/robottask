package youngsters2;

public class Vec2 {
	
	public int x, y;
	
	public Vec2()
	{
		x = y = 0;
	}
	
	public Vec2(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	
	public static boolean isAligned(Vec2 a, Vec2 b, Vec2 c)
	{
		return ((c.x - a.x) / (b.x - a.x) == (c.y - a.y) / (b.y - a.y)); 
	}
}
